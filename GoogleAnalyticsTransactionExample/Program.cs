﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace GoogleAnalyticsTransactionExample
{
    class Program
    {
        private static List<ApiProductAnalytics> Products
        {
            get
            {
                return new List<ApiProductAnalytics>
                {
                    new ApiProductAnalytics
                    {
                        Id = "12345",
                        Brand = "brand-x",
                        Category = "Vehicles",
                        Name = "Bicycle",
                        Dimension2 = "2",
                        Dimension3 = "New",
                        Dimension4 = "150",
                        Dimension5 = "5",
                        Dimension6 = "",
                        Dimension7 = "en-US",
                        Price = "290.00",
                        Quantity = 4,
                        Variant = "Blue"
                    },
                    new ApiProductAnalytics
                    {
                        Id = "12346",
                        Brand = "brand-y",
                        Category = "Vehicles",
                        Name = "Scooter",
                        Dimension2 = "1",
                        Dimension3 = "New",
                        Dimension4 = "190",
                        Dimension5 = "250",
                        Dimension6 = "",
                        Dimension7 = "de-DE",
                        Price = "2599.00",
                        Quantity = 5,
                        Variant = "Red"
                    },new ApiProductAnalytics
                    {
                        Id = "12347",
                        Brand = "brand-z",
                        Category = "Vehicles",
                        Name = "Skateboard",
                        Dimension2 = "10",
                        Dimension3 = "Used",
                        Dimension4 = "20",
                        Dimension5 = "5",
                        Dimension6 = "",
                        Dimension7 = "fr-FR",
                        Price = "55.00",
                        Quantity = 2,
                        Variant = "Gray"
                    },
                    new ApiProductAnalytics
                    {
                        Id = "12348",
                        Brand = "brand-z",
                        Category = "Accessories",
                        Name = "Helmet",
                        Dimension2 = "1",
                        Dimension3 = "New",
                        Dimension4 = "20",
                        Dimension5 = "5",
                        Dimension6 = "",
                        Dimension7 = "fr-FR",
                        Price = "12.50",
                        Quantity = 2,
                        Variant = "Gray"
                    }
                };
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("GA transaction test console");
            Console.WriteLine("Hit Enter key to start...");
            Console.ReadLine();
            Console.WriteLine("Enter your Google Tracking ID:");
            var trackingID = Console.ReadLine();
            
            //Let's first send an event that is registered quickly
            //to see if we have contact.

            var eventPostData = new Dictionary<string, string>
            {
                {"v", "1"},
                {"tid", trackingID},
                {"cid", "555"},
                {"t", "event"},
                {"ec", "Test"},
                {"ea", "Console Action"},
                {"el", "Console connection test"},
                {"ev", "1337"}
            };

            Console.WriteLine("Let's first send an event to check the GA connection");
            Analytics.SendTrackingInformation(eventPostData, "");
            
            var order = new Order()
            {
                Id = DateTime.Now.Hour + DateTime.Now.DayOfYear,
                Products = Products
            };

            var revenue = 14120.00;
            var tax = 2965.20;
            var shipping = 12.30;

            Analytics.AddTransaction(order, DateTime.Now, revenue.ToString("F", CultureInfo.InvariantCulture),
                tax.ToString("F", CultureInfo.InvariantCulture),
                shipping.ToString("F", CultureInfo.InvariantCulture), trackingID);

            Console.WriteLine("Hit Enter key to stop");
            Console.ReadLine();
        }
    }
}
