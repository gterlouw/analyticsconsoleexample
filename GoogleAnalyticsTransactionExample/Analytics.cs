﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Web;

namespace GoogleAnalyticsTransactionExample
{
    class Analytics
    {
        public static void AddTransaction(Order order, DateTime orderDate, string revenue, string tax, string shipping, string universalAnalyticsId, string userAgent = "")
        {
            /*
            * https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide 
            * https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters
            * https://developers.google.com/analytics/devguides/collection/protocol/v1/reference
            */
            var transactionPostData = new Dictionary<string, string>
            {
                {"v", "1"},
                {"tid", universalAnalyticsId},
                {"cid", "555"},
                {"dt", "Confirmed Order"},
                {"ec", "Order Flow"},
                {"ea", "Confirmed Order"},
                {"ti", order.Id.ToString("D")},
                {"ta", "Shop"},
                {"id", order.Id.ToString(CultureInfo.InvariantCulture) },
                {"cd8", orderDate.ToString("d")},
                {"tr", revenue},
                {"tt", tax},
                {"ts", shipping},
                {"pa", "purchase"}
            };

            for (var i = 0; i < order.Products.Count; i++)
            {
                var apiProductAnalytics = order.Products[i];

                var prefix = string.Format("pr{0}", i + 1);

                transactionPostData.Add(string.Format("{0}id", prefix), apiProductAnalytics.Id);
                transactionPostData.Add(string.Format("{0}nm", prefix), apiProductAnalytics.Name);
                transactionPostData.Add(string.Format("{0}br", prefix), apiProductAnalytics.Brand);
                transactionPostData.Add(string.Format("{0}ca", prefix), apiProductAnalytics.Category);
                transactionPostData.Add(string.Format("{0}qt", prefix), apiProductAnalytics.Quantity.ToString("D"));
                //Price should be formatted so that it does not contain comma
                transactionPostData.Add(string.Format("{0}pr", prefix), apiProductAnalytics.Price);
                transactionPostData.Add(string.Format("{0}va", prefix), apiProductAnalytics.Variant);
                transactionPostData.Add(string.Format("{0}cd2", prefix), apiProductAnalytics.Dimension2);
                transactionPostData.Add(string.Format("{0}cd3", prefix), apiProductAnalytics.Dimension3);
                transactionPostData.Add(string.Format("{0}cd4", prefix), apiProductAnalytics.Dimension4);
                transactionPostData.Add(string.Format("{0}cd5", prefix), apiProductAnalytics.Dimension5);
                transactionPostData.Add(string.Format("{0}cd6", prefix), apiProductAnalytics.Dimension6);
                transactionPostData.Add(string.Format("{0}cd7", prefix), apiProductAnalytics.Dimension7);
            }

            SendTrackingInformation(transactionPostData, userAgent);
        }

        public static void SendTrackingInformation(Dictionary<string, string> postData, string userAgent = "")
        {
            var request = (HttpWebRequest)WebRequest.Create("http://www.google-analytics.com/collect");
            request.Method = "POST";
            request.UserAgent = userAgent;

            var postDataString = postData
               .Aggregate("", (data, next) => string.Format("{0}&{1}={2}", data, next.Key,
                                                            HttpUtility.UrlEncode(next.Value)))
               .TrimStart('&').TrimEnd('&');

            Console.WriteLine("These are the parameters and values that we will send to GA.");
            Console.WriteLine(postDataString);

            // write the request body to the request
            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(postDataString);
            }

            try
            {
                var webResponse = (HttpWebResponse)request.GetResponse();
                if (webResponse.StatusCode != HttpStatusCode.OK)
                {
                    Console.WriteLine("{0}:Google Analytics tracking did not return OK 200", (int)webResponse.StatusCode);
                }
            }
            catch (Exception ex)
            {
                // do what you like here, we log to Elmah
                Console.WriteLine("Failed to send data to Google Analytics for the following Dictionary {0}, Errormessage: {1}", postData, ex.Message);
            }
        }

    }

    class Order
    {
        public int Id { get; set; }
        public List<ApiProductAnalytics> Products { get; set; }
    }

    [DataContract]
    class ApiProductAnalytics
    {
        // Product ID (string).
        [DataMember(Name = "id")]
        public string Id { get; set; }

        // Product name (string).
        [DataMember(Name = "name")]
        public string Name { get; set; }

        // Product category (string).
        [DataMember(Name = "category")]
        public string Category { get; set; }

        // Number per unit
        [DataMember(Name = "dimension2")]
        public string Dimension2 { get; set; }

        // Kwaliteit van het product, als deze niet beschikbaar is, veld leeghouden
        [DataMember(Name = "dimension3")]
        public string Dimension3 { get; set; }

        // Lengte van het product, als deze niet beschikbaar is, veld leeghouden
        [DataMember(Name = "dimension4")]
        public string Dimension4 { get; set; }

        // Gewicht van het product, als deze niet beschikbaar is, veld leeghouden
        [DataMember(Name = "dimension5")]
        public string Dimension5 { get; set; }

        //Verkoopinfo
        [DataMember(Name = "dimension6")]
        public string Dimension6 { get; set; }

        //Land
        [DataMember(Name = "dimension7")]
        public string Dimension7 { get; set; }

        //Naam kweker of naam leverancier. Leeg laten als er geen specifieke kweker is
        [DataMember(Name = "brand")]
        public string Brand { get; set; }

        //Kleur van het product
        [DataMember(Name = "variant")]
        public string Variant { get; set; }

        //Productprice(currency)
        //Price should be formatted so that it does not contain comma
        [DataMember(Name = "price")]
        public string Price { get; set; }

        //Productquantity(number)
        [DataMember(Name = "quantity")]
        public int Quantity { get; set; }
    }
}
